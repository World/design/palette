/* palette.vala
 *
 * Copyright 2019-2020 Zander Brown
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using Gtk;

namespace ZBrown {
	const string[] SECTIONS = {
		N_("Blue"),
		N_("Green"),
		N_("Yellow"),
		N_("Orange"),
		N_("Red"),
		N_("Purple"),
		N_("Brown"),
		N_("Light"),
		N_("Dark"),
	};

	class PaletteRow : Box {
		public string title { get; set; }

		private int next_name = 1;

		construct {
			orientation = HORIZONTAL;
			overflow = HIDDEN;

			add_css_class ("linked");
		}

		public PaletteRow (string title) {
			Object (title: title);
		}

		public void add (PaletteButton btn) {
			btn.title = "%s %i".printf (title, next_name++);

			append (btn);
		}
	}

	class PaletteButton : Button {
		private string _hex = "";
		private Stack image_stack = new Stack();
		private uint timeout_id = 0;

		public string title { get; set; }

		public string hex {
			get {
				return _hex;
			}

			construct set {
				_hex = value;
				tooltip_text = value;
			}
		}

		public bool light {
			get {
				return has_css_class("light");
			}

			set {
				if (value) {
					add_css_class("light");
				} else {
					remove_css_class("light");
				}
			}
		}

		construct {
			visible = true;

			width_request = 32;
			height_request = 32;

			hexpand = true;
			vexpand = true;

			overflow = HIDDEN;

			image_stack.transition_type = CROSSFADE;
			image_stack.transition_duration = 100;
			image_stack.add_named (new Box (VERTICAL, 0), "empty");
			image_stack.add_named (new Image.from_icon_name ("emblem-ok-symbolic"), "ok");
			image_stack.add_named (new Image.from_icon_name ("edit-copy-symbolic"), "copy");
			image_stack.visible_child_name = "empty";
			set_child (image_stack);

			var motion = new EventControllerMotion ();
			motion.enter.connect (() => {
				image_stack.visible_child_name = "copy";
				enter ();
			});
			motion.leave.connect (() => {
				image_stack.visible_child_name = "empty";
				leave ();
			});
			add_controller (motion);

			var drag = new DragSource ();
			drag.prepare.connect (() => {
			    Gdk.RGBA colour;
			    colour.parse (_hex);

			    drag.set_state (CLAIMED);

                return new Gdk.ContentProvider.for_value (colour);
			});
			drag.drag_begin.connect (() => {
		        drag.set_icon (new WidgetPaintable (this), 0, 0);
			});
			add_controller (drag);
		}

		public PaletteButton (string hex, bool light) {
			Object (hex: hex, light: light);
		}

		public signal void enter ();
		public signal void leave ();

		public override void clicked () {
			image_stack.visible_child_name = "ok";
			if (timeout_id != 0) {
				Source.remove(timeout_id);
			}
			timeout_id = Timeout.add_seconds(3, () => {
				image_stack.visible_child_name = "empty";
				timeout_id = 0;
				return Source.REMOVE;
			});
			get_display().get_clipboard().set_value(hex);
		}

		public override void snapshot (Gtk.Snapshot snapshot) {
			Gdk.RGBA colour = {};
			Graphene.Rect bounds;

			colour.parse (hex);

			var width = (float) get_width ();
			var height = (float) get_height ();
			bounds = {{ (float) 0.0, (float) 0.0}, { width, height }};

			snapshot.append_color (colour, bounds);

			base.snapshot (snapshot);
		}
	}

	[GtkTemplate (ui = "/org/gnome/design/Palette/palette.ui")]
	class Palette : ApplicationWindow {
		[GtkChild]
		private unowned Box content;

		private HashTable<string, PaletteRow> sections = new HashTable<string, PaletteRow> (str_hash, str_equal);

		const GLib.ActionEntry[] entries = {
			{ "guidelines", guidelines_action },
			{ "export", export_action },
			{ "about", about },
		};

		construct {
			// Bind the actions
			add_action_entries (entries, this);

			foreach (var section in SECTIONS) {
				var row = new PaletteRow (gettext (section));
				sections.insert (section, row);
				content.append (row);
			}

			var file = File.new_for_uri ("resource:///org/gnome/design/Palette/palette.gpl");
			var regex = /(?<r>\d+)\s+(?<g>\d+)\s+(?<b>\d+)\s+(?<name>\w+)/;
			try {
				var dis = new DataInputStream (file.read ());
				string l;
				while ((l = dis.read_line (null)) != null) {
					var line = l.strip ();
					var c = line.get_char (0);
					if (!c.isdigit ()) {
						continue;
					}

					MatchInfo info;
					if (!regex.match (line, 0, out info)) {
						continue;
					}

					var r = int.parse (info.fetch_named ("r"));
					var g = int.parse (info.fetch_named ("g"));
					var b = int.parse (info.fetch_named ("b"));
					var name = info.fetch_named ("name");

					var use_dark = ((r * 0.299) + (g * 0.587) + (b * 0.114)) > 160;

					var hex = "#%02x%02x%02x".printf (r, g, b);

					var row = sections.get (name);

					if (row == null) {
						warning ("Unknown group %s", name);
						continue;
					}

					var btn = new PaletteButton (hex, use_dark);
					btn.enter.connect (() => {
						title = btn.title;
					});
					btn.leave.connect (() => {
						title = _("Palette");
					});

					row.add (btn);
				}
			} catch (Error e) {
				error ("%s", e.message);
			}
		}

		public Palette (Application app) {
			Object (application: app);
		}

		private void guidelines_action () {
			guidelines.begin ();
		}

		private void export_action () {
			export.begin ();
		}

		// Open the guidelines, triggered by win.guidelines
		private async void guidelines () {
			const string uri = "https://developer.gnome.org/hig/guidelines/app-icons.html";
			var launcher = new Gtk.UriLauncher (uri);
			try {
				yield launcher.launch (this, null);
			} catch (Error e) {
				error ("%s", e.message);
			}
		}

		// Export the palette, triggered by win.export
		private async void export () {
			var dialog = new FileDialog();
			dialog.set_initial_name("GNOME HIG.gpl");
			dialog.set_title(_("Export the Palette"));
			dialog.set_accept_label(_("Export"));
			try {
				var exported_file = yield dialog.save(this, null);
				var palette_file = File.new_for_uri ("resource:///org/gnome/design/Palette/palette.gpl");
				palette_file.copy (exported_file, NONE);
			} catch (Error e) {
				error ("%s", e.message);
			}
		}

		// Show the about dialog, triggered by win.about
		private void about () {
			var authors = new string[] {"Zander Brown", "Bilal Elmoussaoui"};
			var artists = new string[] {"Tobias Bernard"};
			var helpers = new string[] {"Jordan Petridis"};
			var dlg = new Adw.AboutDialog () {
				application_name = Environment.get_application_name(),
				application_icon = Gtk.Window.get_default_icon_name(),
				version = PACKAGE_VERSION,
				copyright = _("Copyright © 2018-20 Zander Brown"),
				license_type = GPL_3_0,
				developer_name = "Zander Brown",
				developers = authors,
				designers = artists,
				translator_credits = _("translator-credits"),
				issue_url = "https://gitlab.gnome.org/World/design/palette/-/issues/new",
			};
			dlg.add_credit_section (_("Kept sane by"), helpers);
			dlg.present (this);
		}
	}

	class Application : Adw.Application {
		construct {
			flags = FLAGS_NONE;
			application_id = "org.gnome.design.Palette";
		}

		public override void activate () {
			if (active_window != null) {
				active_window.show();
			} else {
				var win = new Palette(this);
				win.show();
			}
		}

		public override void startup () {
			base.startup();

			var style_manager = Adw.StyleManager.get_default ();
			style_manager.color_scheme = FORCE_DARK;
		}
	}

	public int main (string[] args) {
		Gtk.Window.set_default_icon_name("org.gnome.design.Palette");

		Intl.setlocale (LocaleCategory.ALL, "");
		Intl.bindtextdomain (GETTEXT_PACKAGE, LOCALE_DIR);
		Intl.bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
		Intl.textdomain (GETTEXT_PACKAGE);

		Environment.set_application_name(_("Color Palette"));

		var app = new Application();
		return app.run(args);
	}
}

